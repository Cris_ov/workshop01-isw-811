# ISW-811 Markdown Vagrant en Windows
*Cristian Oporta villalobos*

# Indice
1. [Requerimientos](#requerimientos)  
2. [Descarga e Instalación de Virtual Box](#virtualbox)  
3. [Descarga e Instalación de Vagrant](#vagrant)   
4. [Creacion de directorios y configuracion para uso de maquinas virtuales](#directorios)  
5. [Instancia de GNU/Linux Debian Bullseye 64 Bits](#debian)  
6. [Instalación de programas para el Servidor Web](#webserver)
7. [Instalación de Apache Server](#lamp)
8. [Configuración de VHosts para hospedar varios sitios](#vhosts)
 

# Requerimientos  

* Visual Studio Code o el editor de texto de tu preferencia
* Git 
* Tener la virtualizacion habilitada en el BIOS  


# VirtualBox  

## Descarga e Instalación de VirtualBox  

***Pre Requerimiento: Tener instalado "Virtual Box"***

1. Nos dirigimos al siguiente Link para iniciar la descarga de
[Virtual Box](https://www.virtualbox.org) y damos Click en el Recuadro **"Download VirtualBox 6.1"**.   
    ![vb](img/VirtualBox/VirtualBox1.png)  
2. Como estamos en Windows, vamos a dar descargar el instalador para los **Windows hosts**.  
    ![vb](img/VirtualBox/VirtualBox2.png)  
3. Elegimos el directorio en el que vamos a guardar el archivo, preferiblemente en la carpeta de **Descargas** y le damos **Guardar**.  
    ![vb](img/VirtualBox/VirtualBox3.png)   
4. Abrimos el archivo que acabamos de descargar y clickeamos en **"Next o Siguiente"**.  
    ![vb](img/VirtualBox/VirtualBox4.png)   
5. Durante las siguientes ventanas que nos aparezcan daremos en **"Next, Yes o Siguiente"** confirmando las configuraciones por defecto para la instalación de VirtualBox.  
    ![vb](img/VirtualBox/VirtualBox5.png)   
6. Damos en **"Next o Siguiente"**.  
    ![vb](img/VirtualBox/VirtualBox6.png)    
7. Damos permisos para instalar las interfaces de red virtuales y damos en **"Yes"**.  
    ![vb](img/VirtualBox/VirtualBox7.png)   
8. Procedemos a la instalacón y damos en **"Install"**.  
    ![vb](img/VirtualBox/VirtualBox8.png)    
9. Esperamos a que finalice la instalación.  
    ![vb](img/VirtualBox/VirtualBox9.png)   
10. Finalmente ya tenemos instalado el programa y podemos proceder a **"Start Oracle VM VirtualBox"**.  
    ![vb](img/VirtualBox/VirtualBox10.png)   
11. Asi se vera VirtualBox abierto y listo para trabajar.  
    ![vb](img/VirtualBox/VirtualBox10.png)   
  
# Vagrant

## Descarga e Instalación de Vagrant
1. Ahora nos dirigimos al siguiente link para descargar el complemento de
[Vagrant](https://www.vagrantup.com).  
    ![Vagrant](img/Vagrant/Vagrant1.png)  
2. Seleccionamos el tipo de binarios que ocupamos segun  nuestro tipo de sistema ya sea **32 Bits** o **64 Bits**, en este caso elegiremos **64 Bits**.  
    ![Vagrant](img/Vagrant/Vagrant2.png)  
3. **Guardamos** el archivo en el directorio deseado preferiblemente Descargas.  
    ![Vagrant](img/Vagrant/Vagrant3.png)  
4. Ejecutamos el programa ya descargado y damos click en ***Next*** para continuar con la instalación.  
    ![Vagrant](img/Vagrant/Vagrant4.png)  
5. Aceptamos los terminos y condiciones con el **CheckBox** y le damos en ***Next***.  
    ![Vagrant](img/Vagrant/Vagrant5.png)  
6. A continuacion podremos elegir el destino de instalacion del Vagrant por lo cual dejaremos en la ruta por defecto y daremos en ***Next***.  
    ![Vagrant](img/Vagrant/Vagrant6.png)  
7. Seguidamente Daremos Click en ***Install*** y nos aparecera un PopUp solicitando permisos de administrador que vamos a otorgar para la instalacion de Vagrant.  
    ![Vagrant](img/Vagrant/Vagrant7.png)  
8. Esperamos mientras finaliza la instalacion.  
    ![Vagrant](img/Vagrant/Vagrant8.png)  
9. Finalmente daremos click en ***Finish***  
    ![Vagrant](img/Vagrant/Vagrant9.png)  
10. Por ultimo Reiniciaremos nuestro equipo para que las configuraciones de instalacion hagan efecto en el sistema.  
    ![Vagrant](img/Vagrant/Vagrant10.png)  


# Directorios


## Creacion de directorios y configuracion para uso de maquinas virtuales.

- Nos dirijimos al directorio donde seva a realizar la creacion de los VM's y le damos click derecho para abrir el **"GIT BASH HERE"**.  
  
    ![Bash](img/GIT/GitBashHere.png)  

- Se nos va a abrir un Bash de GIT el cual puede ser usado para trabajar nuestras maquinas virtuales de Vagrant  

- Y escribimos el siguiente comando con el cual creamos el directorio donde se guardaran las Maquinas Virtuales (**VMs**):  
  
    ```bash
    $ mkdir VMs
    ```  
    ![Bash](img/GIT/MKdir.png)  

- Nos traladamos a la carpeta con el siguiente comando:  

    ```bash
    $ cd VMs
    ```  
    ![Bash](img/GIT/cdVms.png)  

- Ahora nos dirigiremos de nuevo a la pagina principal de [Vagrant](https://www.vagrantup.com) y damos click en **"Find Boxes"**  
    ![Vagrant](img/Vagrant/Vagrant11.png)  

- En el buscador digitamos **Bullseye64** y esogeremos para este ejemplo el primer resultado **debian/bullseye64**  
    ![Vagrant](img/Vagrant/Vagrant12.png)  
- Nos colocaremos en la pestaña **New**  
    ![Vagrant](img/Vagrant/Vagrant15.png)  
- Volvemos al ***Bash*** y creamos una carpeta "webserver" el cual sera el nombre de nuestro servidor local dentro de la carpeta "VMs" con:   
    ```bash
    $ mkdir webserver
    ```  
- Nos traladamos a la carpeta con el siguiente comando:  

    ```bash
    $ cd webserver
    ```  
    ![Bash](img/GIT/cdwebserver.png)  

-  Ejecutaremos los siguientes comandos en el bash que dejamos abierto en la carpeta **webserver**   
    ```bash
    $ vagrant init debian/bullseye64
    ```  
    ![Bash](img/GIT/VagrantInit.png)  
- Y nos creara un archivo de [Vagrantfile](VMs/webserver/Vagrantfile) de configuracion que podramos abrir en este caso con ***VSCode*** o con el editor de su preferencia.  
    ![Bash](img/GIT/vagrantfile.png)  

- Vamos a modificar el Archivo Vagrantfile habilitando la linea de configuracion para que la maquina cree su propia red conocida y guardamos el archivo.  
    ![Bash](img/GIT/ezgif.com-gif-maker.gif)  

- Ahora creamos una carpeta **sites** el cual sera una carpeta compartida entre el servidor y la maquina local en la carpeta [webserver](VMs/webserver).  
    ```bash
    $ mkdir sites
    ```  
- Y nos traladamos a la carpeta.  
    ```bash
    $ cd sites
    ```  

- Ageregamos al archivo  [Vagrantfile](VMs/webserver/Vagrantfile) la siguiente linea el cual permite configurar la ruta de la carpeta compartida entre nuestra pc y la maquina virtual y guardamos el archivo: 
    ```bash
        config.vm.synced_folder "sites/", "/home/vagrant/sites", owner:"www-data", group: "www-data"
    ```  
    ![Bash](img/GIT/sites.gif)  


# Debian  

## Instancia de GNU/Linux Debian Bullseye 64 Bits
- Una vez lista la configuracion anterior con el siguiente comando en el "Git Bash" levantamos la instancia de Debian Bullseye:  
    ```bash
    $ vagrant up
    ```  
    > **Nota**: Este comando se tiene que ejecutar dentro del directorio ***VMs/webserver***  
  - Al ser la primera vez, este comando va a descargar la instancia:  
    ![Bash](img/GIT/vagrantup.png)
- En un nuevo explorador de archivos nos trasladamos a la siguiente ruta : 
    ```bash
     C:/Windows/System32/drivers/etc
    ```  
    ![Bash](img/GIT/etchost.png)  
- y abriremos el archivo [hosts](C:/Windows/System32/drivers/etc/hosts) y añadiremos la siguiente linea sin comentar: 
    ```bash
        192.168.56.10   webserver   webserver.local
    ```  
    ![Bash](img/GIT/Hosts.gif)  

- Y si hacemos ping a webserver podra ser capaz de comunicarse y enviar paquetes:  
    ```bash
    ping webserver
    ```  
    ![Bash](img/GIT/ping.png)  

# Conexion

- Para conectarnos a la maquina virtual de Vagrant, lo haremos por ssh usando el siguiente comando  
    ```bash
    vagrant ssh
    ```  
> Nos va a permitir conectarnos y autenticar mediante una llave privada de ssh sin contraseña a la maquina virtual.

# Personalizar Bullseye

- Vamos a cambiarle el nombre de la maquina con el siguiente comando:  
    ```bash
    sudo hostnamectl set-hostname webserver
    ```  
> Los cambios se veran aplicados al cerrar la sesion de  vagrant

- Nos dirijimos a editar desde la maquina virtual con "nano" el nombre del host     
    ```bash
    sudo nano /etc/hosts
    ```  
    * Y cambiamos el apartado que dice **bullseye** por **webserver**  
    ![Bash](img/GIT/hostsnano.gif)  
- Para guardar los cambios usamos la combinacion de teclas ***CTRL + O*** y damos ***ENTER***, luego para salir usamos la combinacion ***CTRL + X***.  
- Cerraremos la sesion de la maquina virtual para que los cambios hechos tomen efecto con el siguiente comando:  
    ```bash
    exit
    ```  
- Ademas de conectarnos con el comando "vagrant ssh" tambien se puede utilizar de la siguiente manera con una llave privada dirigiendonos dentro de la carpeta [webserver](VMs/webserver) a:  
    ```bash
    cd .vagrant
    ```  
    > ahi podremos encontrar en el directorio el **private_key** que se utilizara mas adelante.  

    ![Bash](img/GIT/privatekey.png)  

- Y nos aseguramos que esta corriendo un agente de sssh que pueda reconocaer la llave privada con el siguiente comando:  
    ```bash
    eval "$(ssh-agent -s)"
   ```  
    - y automaticamente carga un agente aunque no estuviera inicializado.  
    ![Bash](img/GIT/agent.png)   

- Lo siguiente es indicarle al sistema que reconozca la llave privada que se encuentra en el directorio **".vagrant"** con el comando:
    ```bash 
    ssh-add .vagrant/machines/default/virtualbox/private_key
    ```  
    ![Bash](img/GIT/keyadded.png)   
-  y ahora continuaremos con la conexion al server con el siguiente comando:
    ```bash 
    ssh vagrant@webserver
    ``` 
    ![Bash](img/GIT/sshconnect.png)   

# Webserver

## Instalación de programas para el Servidor Web

- Antes de instalar cualquier nueva herramienta debemos actualizar nuestra maquina con el comando:
    ```bash
    sudo apt-get update
    ``` 
    ![Bash](img/GIT/update.gif)   

- Despues que las actualizaciones sean hechas nos dirigimos a instalar las siguientes herramientas **"git, vim, vim-nox y curl**":
    ```bash
    sudo apt-get install git vim vim-nox curl 
    ```
- Acontinuacion nos va a preguntar si deseamos continuar con la intalacion y escribimos **"Y"** (Yes)

    

- Ahora con lo necesario intalado haremos modificaciones al archivo sources lists que nos permitan obtener paquetes de las 3 areas de distribucion de paquetes **main, contrib y non-free.**  
    ```bash
     sudo vim /etc/apt/sources.list
    ```
    ![Bash](img/GIT/sourceslist.png)   

- Con el siguiente comando reemplazaremos los permisos para total acceso a los paquetes:
    ```bash
     :%s/main/main contrib non-free/g 
    ```
    ![Bash](img/GIT/sourcesmod.png)   

- Tambien se comentara los lineas que continenen **src** con un **#** al inicio de la linea ingresando al modo de insercion con la tecla **i**.
    ![Bash](img/GIT/srccomment.png)   

- Por ultima para salir de la edicion damos en **ESC** y digitando **:w** se guarda y para salir digitamos **:q** salir  
     > podemos usar tambien **:wq** para guardar y salir
- Volvemos a ejecutar el comando para actualizar los paquetes que hayamos instalado anteriormente:
    ```bash
     sudo apt-get update
    ```
# LAMP  
## Instalacion de Apache server  
- Descargaremos la siguiente Herramienta con el comando:    
    ```bash
    sudo apt-get install apache2
    ```  
- Acontinuacion nos va a preguntar si deseamos continuar con la intalacion y escribimos **"Y"** (Yes)  


- Desde este momento una vez instalado podriamos accesar desde el navegador de nuestra pc anfitrion con http://webserver a la pagina principal de apache  
    ![Bash](img/GIT/apache.png)     
    
- Para editar el archivo de inicio de html se modifica el archivo que esta en:   
    ```bash
    cd var/www/html/
    ```  
    ![Bash](img/GIT/treeindex.png)     

- Podremos modificarlo con:
    ```bash
    echo "<h1>Hola mundo</h1>" | sudo tee index.html
    ```  

- O modificarlo completo con:
    ```bash
    sudo vim index.html 
    ```  
    > Y de la misma manera una vez modificado guardarlo con **:wq**  
    
- En la carpeta **/vagrant/sites** de la maquina virtual creamos una carpeta:
    ```bash
    mkdir webserver.com
    ```  

- Añadimos un archivo a la carpeta:
    ```bash
    touch "<h1>Hola mundo</h1>" > webserver.com/index.html 
    ```  
- Instalar **Tree** nos puede ayudar a ver mejor la manera en la que estan los directorios acomodados.  
    ```bash
    sudo apt-get install tree
    ```  
    ![Bash](img/GIT/tree.png)     

# VHosts
## Creacion de Virtuals Hosts (VHosts)
- En **/vagrant** en la maquina anfitrion creamos un archivo **webserver.conf** y lo editamos con **VsCode**.  
    ```bash
    <VirtualHost *:80>
    ServerAdmin webmaster@isw811.xyz
    ServerName webserver.local

    # Indexes + Directory Root.
    DirectoryIndex index.html
    DocumentRoot /home/vagrant/sites/webserver.com

    <Directory /home/vagrant/sites/webserver.com>
        DirectoryIndex index.html
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/webserver.com.error.log
    

    # Possible Values include: debug, info, notice,warn, error, crit
    # alert, emerg
    LogLevel warn

    CustomLog ${APACHE_LOG_DIR}/webserver.com.access.log combined
    </VirtualHost>
    ```  

- Habilitar hosts
    ```bash
    sudo a2enmod vhost_alias rewrite
    ```  

- Reiniciar apache
    ```bash
    sudo systemctl restart apache2
    ```  

## Configuración de archivos hosts para simular la resolución de los dominios  

- en la maquina virtual 
    ```bash
    cd /vagrant/
    ```  

- Copiamos el archivo webserver.conf a la carpeta sites-available de apache    
    ```bash
    sudo cp *.conf /etc/apache2/sites-available/
    ```   
- habilitar la configuracion de los vhosts  
    ```bash
    sudo a2ensite webserver  
    ```  

- Reinicio apache
    ```bash 
    sudo systemctl restart apache2
    ```  
- ahora configuraremos la directiva **"ServerName"** en el siguiente archiovo:
    ```bash
    sudo vim /etc/apache2/apache2.conf
    ```  
- Al final del archivo con la tecla "i" entraremos en el modo de insercion y añadiremos la siguiente linea:
    ```bash
    ServerName  webserver
    ```  
 - Con el siguiente comando revisaremos que no haya errores en la configuracion revisando la sintaxis:
    ```bash
    sudo apache2ctl -t
    ```  
    ![Bash](img/GIT/syntax.png)     

- Y por ultimo Recargaremos Apache
    ```bash 
    sudo systemctl reload apache2
    ```  
- Ya con esto podriamos ser capaces de trabajar nuestro servidor web basico.
http://webserver.local

## Servidor Web Funcionando  
![Bash](img/GIT/webserver.png)     
